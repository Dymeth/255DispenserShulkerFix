package ru.dymeth.dispensershulkerfix;

import org.bukkit.block.Block;
import org.bukkit.block.BlockFace;
import org.bukkit.block.ShulkerBox;
import org.bukkit.event.EventHandler;
import org.bukkit.event.EventPriority;
import org.bukkit.event.HandlerList;
import org.bukkit.event.Listener;
import org.bukkit.event.block.BlockDispenseEvent;
import org.bukkit.inventory.meta.BlockStateMeta;
import org.bukkit.material.Dispenser;
import org.bukkit.plugin.java.JavaPlugin;

public class DispenserShulkerFix extends JavaPlugin implements Listener {
    @Override
    public void onEnable() {
        this.getServer().getPluginManager().registerEvents(this, this);
    }

    @Override
    public void onDisable() {
        HandlerList.unregisterAll((Listener) this);
    }

    @EventHandler(priority = EventPriority.LOWEST, ignoreCancelled = true)
    private void on(BlockDispenseEvent event) {
        if (!(event.getItem().getItemMeta() instanceof BlockStateMeta)) return;
        if (!(((BlockStateMeta) event.getItem().getItemMeta()).getBlockState() instanceof ShulkerBox)) return;
        if (event.getBlock().getY() != (event.getBlock().getWorld().getMaxHeight() - 1)
                && event.getBlock().getY() != 0) return;
        if (((Dispenser) event.getBlock().getState().getData()).getFacing() !=
                (event.getBlock().getY() == 0 ? BlockFace.DOWN : BlockFace.UP)) return;
        event.setCancelled(true);
        Block block = event.getBlock();
        this.getLogger().warning("Предотвратили попытку крашнуть сервер раздатчиком с шалкером: "
                + block.getWorld().getName() + " " + block.getX() + " " + block.getY() + " " + block.getZ());
    }
}
